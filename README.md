GUI expects to be connected to the same network as the boom wifi dongle
in order to begin operation.
The desired_net[work] can be changed in the checkCXN function.
The GUI relays the options that the DUE data logger has available for 
data collection such as:
 ('l') list SD card data
 ('w') wipe SD card data
 ('r') record data
 ('t') test data over USB 
    - need to have a cable connected to the micro usb nearest to the power 
     supply port and open a serial port window 
    (any will do, Arduino COM port is easy)
  Download files via button or by inputing the number corresponding to the file
  when you press the ('l') command to review the SD card data.
  **After each command, you should the menu of commmand options ALWAYS reappear.
  If not there's an issue and you may need to reset the system via power cycle.*
  
  When downloading file, the GUI assumes that the DUE is sending 9 variables
  (1 timer, 2 encoders, and 6 MBLC variables).
  If that structure is to change, the same must be reflected in the MBLC code,
  and the DUE code. Otherwise you will collect junk MBLC data.
  
  NOTE: Currently at 9 variables, over 100 KB of data is collected quickly
        and the wifi data transmission from the wifi dongle is not quite stable
        enough to handle that bandwith. 
        You may need to power cycle the whole system (DUE and wifi dongle) 
        GUI logger is typically fine unless MATLAB throws error.
 