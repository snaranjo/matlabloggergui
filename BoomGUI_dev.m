function varargout = BoomGUI_dev(varargin)
% BOOMGUI_DEV MATLAB code for BoomGUI_dev.fig
%      BOOMGUI_DEV, by itself, creates a new BOOMGUI_DEV or raises the existing
%      singleton*.
%
%      H = BOOMGUI_DEV returns the handle to a new BOOMGUI_DEV or the handle to
%      the existing singleton*.
%
%      BOOMGUI_DEV('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ?BOOMGUI_DEV.M with the given input arguments.
%
%      BOOMGUI_DEV('Property','Value',...) creates a new BOOMGUI_DEV or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BoomGUI_dev_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BoomGUI_dev_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BoomGUI_dev

% Last Modified by GUIDE v2.5 27-Sep-2018 11:37:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BoomGUI_dev_OpeningFcn, ...
                   'gui_OutputFcn',  @BoomGUI_dev_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
%     handles = guidata(hObject);
%     if(handles.closeGUI)
%         guiFig_CloseRequestFcn(handles.guiFig);
%     end
        
end
% End initialization code - DO NOT EDIT


% --- Executes just before BoomGUI_dev is made visible.
function BoomGUI_dev_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BoomGUI_dev (see VARARGIN)

% Choose default command line output for BoomGUI_dev
handles.output = hObject;
% data = guidata(hObject);
handles.connect = false;
handles.terminate = false;
handles.echo = false;
handles.localPort = 0;
handles.msgBox.String = '';
handles.dwnldFileNum.String = '';
handles.cxnStatus.Value = 0;
handles.textStatus.String = '';
handles.uitable1.Data = cell(50,2);
% handles.guiFig.Position = [5  23.0000   89.1667   26.8000]; %primary monitor top-left
% handles.guiFig.Position = [-207 10.87 89.17 26.80]; %2nd monitor

% handles.guiFig.Position = [8.167 23.467 89.167 26.80]; %primary monitor

if ~license('test','instr_control_toolbox')
    errorMsg = 'Do not have Instrumentation Control Toolbox';
%     error(['\n' errorMsg '\n']);
    errordlg(errorMsg,'GUI ERROR');
%     guiFig_CloseRequestFcn(handles.guiFig);
    handles.closeGUI = true;
else 
    handles.closeGUI = false;
end
clearUDP_obj;
handles = setupUDP(handles);
% guidata(hObject, data);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BoomGUI_dev wait for user response (see UIRESUME)
% uiwait(handles.guiFig);


% --- Outputs from this function are returned to the command line.
function varargout = BoomGUI_dev_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function msgBox_Callback(hObject, eventdata, handles)
% hObject    handle to msgBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of msgBox as text
%        str2double(get(hObject,'String')) returns contents of msgBox as a double
    inputText = get(hObject,'String');
    hdl = guidata(hObject);
    
    if iscell(inputText)
        inputText = inputText{:};
    end
    if hdl.echo

        fprintf('INPUT: %s\n',inputText);
    end
    
    hdl.uitable1.Data = updateCol(hdl.uitable1.Data, inputText, 2);
    set(hObject,'String','');
    if strcmp(handles.u.Status,'closed'), fopen(handles.u); end
    fprintf(handles.u, '%s', inputText);
    hdl.textStatus.String ='';
    guidata(hObject,hdl);

% --- Executes during object creation, after setting all properties.
function msgBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to msgBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
    set(hObject,'String','');
    guidata(hObject);

end


 function newTbl = updateCol(currentTbl, newMsg, col)

currentTbl(:,col) = circshift(currentTbl(:,col),1);
currentTbl{1,col} = newMsg;
newTbl = currentTbl;

% --- Executes on button press in cxnStatus.
function cxnStatus_Callback(hObject, eventdata, handles)
% hObject    handle to cxnStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cxnStatus
h           = guidata(hObject);
desiredStat = get(hObject,'Value');

if desiredStat && checkNetCXN
    if ~isfield(h,'u')
        h = setupUDP(h);
    end
    fopen(h.u);
    if ~h.localPort, h.localPort = h.u.LocalPort; end 
    fprintf(h.u,'%s','nn');
    h.textStatus.String = 'Connection started';
else
%     instr = instrfind;
%     fclose(instf
    clearUDP_obj;
    if isfield(h,'u'), h = rmfield(h,'u'); end
    h.textStatus.String = 'Connection terminated';
    h.cxnStatus.Value = 0;
end

h.connect   = desiredStat;

if h.echo
    if desiredStat
        disp('Connection started');
    else
        disp('Connection terminated');
    end
end
% hObject.Value = 1;
guidata(hObject, h);

function receivedUDPmsg (hObject, eventdata, handles)
% Function will be attached to UDP callback and will register the output
% to the table in the GUI

msg = char(fscanf(hObject));
h   = findall(groot,'Type','Figure'); % Get all figures
gui = h(cellfun(@(x) strcmp(x,'guiFig'),{h.Tag})); %find guiFig
hdl = guidata(gui);

tbl = hdl.uitable1.Data;
%Look for \n and format into separate lines in output table
% cell_msg = regexp(msg, '.*','match','dotexceptnewline');
cell_msg = regexp(msg, '[\f\r\n]','split');

for i = 1: length(cell_msg)
    if ~isempty(cell_msg{i})
        if length(cell_msg{i}) == 1
            tbl{1,1} = [tbl{1,1} cell_msg{i}];
        else
            tbl = updateCol(tbl, cell_msg{i}, 1);
        end
%         tbl(:,1) = circshift(tbl(:,1),1);
%         tbl{1,1} = cell_msg{i};
    end
end
% tbl(:,1) = circshift(tbl(:,1),1);
% tbl{1,1} = msg;
hdl.uitable1.Data = tbl;
clear tbl;
hdl.textStatus.String ='';
guidata(gui, hdl);

function hdl = setupUDP(hdl)
%% UDP setup
uIP = '192.168.1.158'; %.0.19'; FEED-MI  %1.158'; robot %1120C vicon net
% uIP = '192.168.0.19'; %.0.19'; FEED-MI  %1.158'; robot %1120C vicon net
uPort = 5420;
inputlimit = 1000;

if isfield(hdl, 'u') && hdl.localPort > 0
   if strcmp(hdl.u.status, 'open') && isempty(hdl.u.DatagramReceivedFcn)
        fclose(hdl.u);
        set   (hdl.u,'DatagramReceivedFcn',@receivedUDPmsg);
        fopen (hdl.u);
%         disp('Returned callback receivedUDPmsg');
   end
else
     hdl.u = udp(uIP, uPort, ...
                'InputDatagramPacketSize', inputlimit,...
                'InputBufferSize'        , inputlimit,...
                'ByteOrder'              , 'littleEndian',...
                'DatagramReceivedFcn'    , @receivedUDPmsg);
end
    
% hdl.localPort = hdl.u.LocalPort;
clear uIP uPort inputlimit;

function clearUDP_obj()
x = instrfind; 
if ~isempty(x)
    ind = strcmp(x.Status,'open');
    
    if (ind > 0) , fclose(x(ind)); end
   
    delete(x);
    clear ind;
end
clear x;

% --- Executes when user attempts to close guiFig.
function guiFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to guiFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
clearUDP_obj;
delete(hObject);

function saveLocation_Callback(hObject, eventdata, handles)
% hObject    handle to saveLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of saveLocation as text
%        str2double(get(hObject,'String')) returns contents of saveLocation as a double
h = guidata(hObject);

h.SaveLoc = get(hObject,'String');
if ~exist(h.SaveLoc,'dir')
    mkdir(h.SaveLoc);
end
guidata(hObject, h);

% --- Executes during object creation, after setting all properties.
function saveLocation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to saveLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
    
end
h = guidata(hObject);
set(hObject,'String',cd);
h.SaveLoc = cd;

guidata(hObject, h);

function [ts, dat] = processData(h, x, titleName)
%% processData accepts a 1-D binary array, x, with titleNae
%  Assuming the user knows the format of the data, the binary 
%  will be assembled under the variables they are expected to be.

boomVars = 3; % time & 2 encoders
mblcVars = 6; % number of output variables from MBLC
singleVars = boomVars-1 + mblcVars; %avoid time variable

rowBytes = 4*(boomVars + mblcVars); %bytes per row of data
rowsPerBlock = floor((512-4)/rowBytes);
count = length(x)-1;
rows = cast(count/rowBytes,'uint32');
bCnt    = 1;
rCnt    = 1;
blckCnt = 1;
B = x;
datcnt = []; t = []; fil = [];
while(bCnt < count) 
    datcnt(blckCnt) = typecast(B(bCnt:bCnt+1),'uint16'); 
    bCnt = bCnt+2;
    overrun(blckCnt) = typecast(B(bCnt:bCnt+1),'uint16'); 
    bCnt = bCnt+2;
    
    if datcnt(blckCnt) <= rowsPerBlock
        rowLimit = datcnt(blckCnt);
    else 
        rowLimit = rowsPerBlock;
    end
for i = 1: rowLimit
    
   t(rCnt)     = typecast(B(bCnt:bCnt+3),'uint32'); 
   bCnt        = bCnt + 4;
   dat(rCnt,:) = typecast(B(bCnt:bCnt+(singleVars*4-1)),'single'); 
   bCnt        = bCnt + (singleVars)*4; 
   rCnt        = rCnt + 1;
   
end
remBytes = count-bCnt;
    if remBytes > 4 && remBytes < 512
        %rest is considered garbage
        rnge = bCnt:count;
        final = typecast(B(bCnt:end),'uint8');
        bCnt = bCnt + length(rnge);
        blckCnt = blckCnt - 1;
    else 
      fil(blckCnt,:) = typecast(B(bCnt:bCnt+3),'uint8'); 
      bCnt    = bCnt + 4;
    end
    blckCnt = blckCnt + 1;
end
t(rCnt:end) = [];
dat(rCnt:end,:) = [];
% Need to look at how the last block is handled because it tends to shift 
%  the data...Not a massive issue but I'd like to sort out.
if ishandle(2), close(2); end
ts = t-t(1); ts = single(ts)./1000;
figure(2); hold on;
f = gcf; f.Name = titleName;
cols = ceil((singleVars)/2);

subplot(2,cols,1), plot(ts, dat(:,1)), grid on, title('encoder0 [deg]');
subplot(2,cols,cols+1), plot(ts, dat(:,2)), grid on, title('encoder1 [deg]');

plotID = 1:8; plotID([1 cols+1]) = [];
for i = 1:mblcVars
    subplot(2,cols,plotID(i)), plot(ts, dat(:,i+2)), ...
            grid on, title(['mblc[' num2str(i-1) ']']);
end
hold off;
% figure(3);  plot(ts, '.-'); 
% Save data locally----------------------------------
saveName = strrep([titleName '.mat'],'!','_Run');
saveName = findUniqueName(h.SaveLoc, saveName);
s = struct(); s.t = ts; s.d = dat;
save(fullfile(h.SaveLoc,saveName), 's');
h.textStatus.String = ['Saved as ' saveName];
drawnow


function newFilename = findUniqueName(folder, fileName)
    %Confirm filename does not already exist
    dirList = dir(folder);
    filesCntr = length(dirList);
    noMatch = 1;
    while noMatch && (filesCntr > 0)
        if strcmp(dirList(filesCntr).name, fileName)
            noMatch = 0;
%            disp('Found match!');
           %determine which run iteration
           runNum = str2double(fileName(end-5:end-4)); %end _Run00.mat
           if runNum < 9
               fileName(end-5:end-4) = ['0' num2str(runNum+1)];               
           else
               fileName(end-5:end-4) = num2str(runNum+1);               
           end
           %Confirm no other runs exist
           fileName = findUniqueName(folder,fileName);
        end %if found similar file name
        filesCntr = filesCntr - 1;
    end
    newFilename = fileName;
    
% --- Executes on button press in clearTable.
function clearTable_Callback(hObject, eventdata, handles)
% hObject    handle to clearTable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = guidata(hObject);
h.uitable1.Data = cell(50,2);
h.textStatus.String = '';
guidata(hObject, h);

function [x, h, saveName] = downloadFile( h, choiceNum)
%% Procedure to download a file, given the handles of the gui
% Stop the recevied UDP call back fun
    h.localPort = h.u.localPort;
    fclose(h.u);
    set   (h.u,'LocalPort'          , h.localPort, ...
               'DatagramReceivedFcn', '');
    fopen (h.u);

    x = [];
    saveName = ''; %Initialize saveName 
    % Notify ESP & DUE of file download
    h.textStatus.String = 'Starting download process..';
    drawnow
    
    %Send start char to ESP, ensure it's ready for download
    startDwnld = 0;  t_timeout = tic; cnt = 0;
    while(~startDwnld) 
        fprintf(h.u,'%s','~~'); 
        
        while ~h.u.BytesAvailable && toc(t_timeout) < 5,   end
        t_timeout = tic;
        cnt = cnt + 1;
        bytesAvail = h.u.BytesAvailable;
        if bytesAvail == 1
            rxMsg = char(fread(h.u,bytesAvail,'uchar'));
            if rxMsg == '~'
                startDwnld = 1;
%                 disp('rx: ~');
                break; 
            end
        elseif bytesAvail > 1
            rxMsg = char(fread(h.u,bytesAvail,'uchar'));
            msgStr = sprintf('RX: %s',rxMsg);
            h.textStatus.String = msgStr;
            drawnow
        else
            h.textStatus.String = 'Received nothing...';
            drawnow
            h = setupUDP(h);
            return;
        end

        if cnt > 2
            h.textStatus.String = 'Timeout: Couldn''t start download';
            drawnow
            fprintf(h.u,'%c','M'); 
            h = setupUDP(h);
            return;
        end

    end
    
    reqMsg = sprintf('<%u>',choiceNum);
    fprintf(h.u,'%s',reqMsg);
    
    h.textStatus.String = reqMsg;
    drawnow
    
    t_wait = tic;
    %~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    while ~h.u.BytesAvailable
        if toc(t_wait) > 5 %ESP-DUE should respond quickly, otherwise cancel
            cancelMsg = sprintf('No response, may need to hard reset');
            fprintf(['\n' cancelMsg '\n\n']);
            h.textStatus.String = cancelMsg;
            drawnow
            h = setupUDP(h);
            return;
        end
    end
    
    msgStr = char(fscanf(h.u));
    if ~isempty(strfind(msgStr,'<E>')) || isempty(strfind(msgStr,'><'))
        cancelMsg = sprintf('%s || improper format "><"',msgStr);
        fprintf(['\n' cancelMsg '\n\n']);
        h.textStatus.String = cancelMsg;
        drawnow
        h = setupUDP(h);
        return;
    end
     msgNums = str2double(regexp(msgStr,'\d*','Match'));
    [~, saveName] = strtok(msgStr,'><');
    if ~isempty(saveName)
        saveName = saveName(3:end-5); 
         updateMsg = sprintf('Downloading: %s',saveName);
         h.textStatus.String = updateMsg;
         h.uitable1.Data     = updateCol(h.uitable1.Data, updateMsg, 1);
         drawnow
    else
        cancelMsg = sprintf('Didn''t find proper format for filename');
        fprintf(['\n' cancelMsg '\n\n']);
        h.textStatus.String = cancelMsg;
        drawnow
        h = setupUDP(h);
        return;
 
    
    end
    
%     fprintf(['\n Rx msg: ' msgStr '\n\n']);
    
    %ESP is expecting the fileSize to confirm receipt
    fprintf(h.u,'<%d>',msgNums(1)); %First number should contain size
    bytesRx = 0;
    ctr = 0; t = tic; tot = tic;
    
    while length(x) < msgNums(1) && toc(t) < 15 %bytesRx < msgNums(1)
        bytesAvail = h.u.BytesAvailable;
        if bytesAvail > 0
            rxBuffer = fread(h.u,bytesAvail,'uchar');
            rxMSG    = sprintf('<%d>',bytesAvail);
            x        = [x; uint8(rxBuffer)];
            bytesRx  = bytesAvail+bytesRx;
            fprintf(h.u,'%s',rxMSG);
            if ctr == 10 %Update GUI indicator
                ctr = 0;
                updateMsg = sprintf('%03.1f%%..'   , 100*bytesRx/msgNums(1));
                h.textStatus.String = updateMsg;
                drawnow
            else
                ctr = ctr+1;
            end
            t = tic;
        end
        
    end
   
    finalMsg = sprintf('Received %05.2f %% of file in %6.2f sec.',...
                        (length(x)/msgNums(1)*100));
%     fprintf(['\n' finalMsg '\n\n']);
    h.textStatus.String = finalMsg;
    if length(x) < msgNums(1)
        wrnMSG = sprintf('WARNING: Did not get entire message! %05.2f %%',...
                          length(x)/msgNums(1)*100);
        h.textStatus.String = [finalMsg ' |' wrnMSG];
%         fprintf(['\n' wrnMSG '\n\n' ]);
        fprintf(2,['\n' wrnMSG '\n\n']);
%         warning(wrnMSG);
%         fprintf('\n\n');
    end

    while (h.u.BytesAvailable > 0)
         fprintf(char(fread(h.u,h.u.BytesAvailable))); 
    end
    h = setupUDP(h);
% --- Executes on button press in dwnLast.
function dwnLast_Callback(hObject, eventdata, hdl)
% hObject    handle to dwnLast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = guidata(hObject);

% if(h.connect)
%    if(h.echo), fprintf('%s\n','Connect is high!'); end
% end
% 
% if(h.terminate)
%    if(h.echo), fprintf('%s\n','Terminate is high!'); end
% end   
h.textStatus.String = 'Downloading latest on SD...';
drawnow

[x, h, saveName] = downloadFile( h, 6000);

guidata(hObject,h);
if exist('x','var')
    if ~isempty(x), processData(h, x , saveName); end
end

function dwnldFileNum_Callback(hObject, eventdata, handles)
% hObject    handle to dwnldFileNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dwnldFileNum as text
%        str2double(get(hObject,'String')) returns contents of dwnldFileNum as a double
h = guidata(hObject);
requestNum = str2double(get(hObject,'String'));

if(~isnumeric(requestNum) || isnan(requestNum))
    h.textStatus.String = 'Received non-numeric request. Cancelling..';
    drawnow
    return;
else
    replyMsg = sprintf('Received %u input', requestNum);
    h.textStatus.String = replyMsg;
    drawnow
    pause(0.5);
    [x, h, saveName] = downloadFile( h, requestNum);
end
% pause(0.5);
% if(h.connect)
%    if(h.echo), fprintf('%s\n','Connect is high!'); end
% end
% 
% if(h.terminate)
%    if(h.echo), fprintf('%s\n','Terminate is high!'); end
% end   
% disp('ready to clear str..');
set(hObject,'String','');
h.textStatus.String = '';
drawnow
guidata(hObject,h);
if exist('x','var')
    if ~isempty(x)
        processData(h, x , saveName);
%         saveName
%         disp('got data');
    end
end

% --- Executes during object creation, after setting all properties.
function dwnldFileNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dwnldFileNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in saveDLGbutton.
function saveDLGbutton_Callback(hObject, eventdata, handles)
% hObject    handle to saveDLGbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = guidata(hObject);

saveDir = uigetdir;
if ischar(saveDir)
    h.SaveLoc = saveDir;
    h.saveLocation.String = saveDir;
    drawnow
    guidata(hObject, h);
end

% --- Fun to check machine network connection and warn if not connected
function connected = checkNetCXN ()
desired_net = 'vicon1';

if ispc 
    
%     desired_net = 'vicon1';
    [~,net_char] = system('netsh wlan show profiles'); %will show SSID profiles. 
    prof_Ind     = strfind(net_char,'All User Profile');      %First is connected SSID.
    [network, ~] = strtok(net_char(prof_Ind(1)+23:prof_Ind(2)), char(10));
    
else
    %If running fron mac - method works but is not fool-proof
%     desired_net = 'FEED-MI';
    sys_cmd = ['$(/System/Library/PrivateFrameworks/Apple80211.framework/'...
        'Versions/Current/Resources/airport -I | sed -e "s/^  *SSID: ' ...
        '//p" -e d)'];
    [~, net_char] = system(sys_cmd);
    colon_ind = strfind(net_char,':');
    network = net_char(colon_ind(1)+2:colon_ind(2)-1);
end

if ~strcmp(network, desired_net)
    warnMsg = ['Connected to ' network '. Must connect to ' desired_net '!'];
               
    fprintf(2,[warnMsg ...
             '\nConfirm that WiFi dongle is connected as well.' ...
             '\nClose warning dialog box and rerun script.\n\n']);
    warndlg(warnMsg);
    connected = 0;
    return;
else
    connected = 1;
end